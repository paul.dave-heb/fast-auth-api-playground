import UIKit

/*
 https://hebecom.atlassian.net/wiki/spaces/DIC/pages/3430320338/RFC-DF-iOS-0008+FAST+-+Authentication+Handling+Pt.+2
 * UI Layer Interaction *
 When it's time for auth (either login or re-auth), The service layer vends a proxy object to the UI layer.
 The proxy object provides a limited API for completing login and conveys the lifetime of the UI interaction to the service.
*/

// MARK: - Services Layer

public protocol AuthenticationManagerDelegate: AnyObject {
    /// Called when AuthenticationManager needs user credentials
    func authenticationManager(_ authManager: AuthenticationManager, requestsReAuthentication proxy: SignInProxy)
}

public struct User {
    public let name: String
}

/// A proxy for the UI
/// Given to the UI layer in one of two ways:
/// * UI calls into AuthenticationManager.login()
/// * AuthenticationManager pushes a session to its delegate for re-auth
public class SignInProxy {
    public let currentUser: User?
    private let authManager: AuthenticationManager

    fileprivate init(authManager: AuthenticationManager, currentUser: User?) {
        self.authManager = authManager
        self.currentUser = currentUser
    }

    /// If there's a current user, the username field is ignored here
    public func submit(username: String, password: String) async throws {
        try await authManager.proxy_submit(username: currentUser?.name ?? username, password: password)
    }

    public func flowDidEnd() {
        authManager.proxy_end()
    }

    deinit {
        flowDidEnd() // don't drop the ball
    }
}


public class AuthenticationManager {
    weak var delegate: AuthenticationManagerDelegate?

    public func login() -> SignInProxy {
        // ... set some internal state, so we can't have more than one session?
        return SignInProxy(authManager: self, currentUser: nil)
    }

    fileprivate func proxy_submit(username: String, password: String) async throws {
        // hey authManager, make a network call to try to login.
        // lemme know how it goes so the UI can show necessary errors, server side validation, etc.
        // ...
    }

    fileprivate func proxy_end() {
        // hey authManager, we're done with the UI. If we submitted a successful name/pw, you can unblock anything waiting for auth.
        // if we didn't, shut it all down!
        // ...
    }

    private func didSomeTokenStuffButCantRefresh() {
        // not included here - guard on delegate, that we have a user, etc.
        // the user would come from identity storage
        delegate?.authenticationManager(self, requestsReAuthentication: SignInProxy(authManager: self, currentUser: .init(name: "Bob")))
    }
}

// MARK: - UI Layer

/// Example of how the UI layer would interact with the proxy.
///  The flow doesn't need any other service APIs besides the proxy to complete login or re-auth.
class LoginFlowController: UIViewController {
    let proxy: SignInProxy

    init(signInProxy: SignInProxy) {
        proxy = signInProxy
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func onLoginTapped() {
        Task {
            do {
                try await proxy.submit(username: "foo", password: "bar")
                onDismiss()
            } catch {
                // ... show error in the UI
            }
        }
    }

    // called on login success, cancel, etc. etc.
    private func onDismiss() {
        dismiss(animated: true) {
            // end the session after animated dismissal
            self.proxy.flowDidEnd()
        }
    }
}
